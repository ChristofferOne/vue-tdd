import { mount } from '@vue/test-utils';
import HelloWorld from '../components/HelloWorld.vue';
import {describe, it} from "mocha";

const wrapper = mount(HelloWorld);

describe('Hello World Test', () => {
    it('Defaults name input field to Name', () => {
        expect(wrapper.vm.$data.name).toBe('Name');
    });

    it('Displays Milad when I write it', () => {
        const input = wrapper.find('#name');
        input.element.value = 'Milad';
        input.trigger('input');

        expect(wrapper.vm.$data.name).toBe('Milad');
    });

    it('Updates the input to the data value', () => {
        wrapper.vm.$data.name = 'Jesper';

        const input = wrapper.find('#name');

        expect(input.element.value).toBe('Jesper');
    });

    it('Counts up when clicking the Add button', () => {
        const input = wrapper.find('#add');
        input.trigger('click');

        expect(wrapper.vm.$data.count).toBe(1);
    });

    it('Counts down when clicking the Sub button', () => {
        const input = wrapper.find('#sub');
        input.trigger('click');

        expect(wrapper.vm.$data.count).toBe(0);
    });

    it('Counts down when clicking the Sub buttonCounts down when clicking the Sub button', () => {
        const input = wrapper.find('#add');
        input.trigger('click');
        input.trigger('click');

        expect(wrapper.vm.$data.count).toBe(2);
    });
});
